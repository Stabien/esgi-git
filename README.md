
# esgi-git

## Description

esgi-git is a project designed to verify the knowledges learned during git course over the first part of the school year.

## Setup

To push on both Github and Gitlab repositories :

```
git remote set-url --add --push origin git@github.com:Stabien/esgi-git.git
git remote set-url --add --push origin git@gitlab.com:Stabien/esgi-git.git
```

## Gitlab repository

https://gitlab.com/Stabien/esgi-git.git

## Authors 

Stabien & Mel-Red